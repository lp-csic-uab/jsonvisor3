; For JsonVisor
; J.Abian 30 july 2010 - december 2012

#define MyAppName ReadIni(SourcePath + "jinstall.ini", "Common", "name", "noname")
#define MyVersion ReadIni(SourcePath + "jinstall.ini", "Common", "version", "0.0.0")
#define MyImageDir ReadIni(SourcePath + "jinstall.ini", "Common", "imgdir", "")
#define MyWizardDir ReadIni(SourcePath + "jinstall.ini","Common", "imgdir", "")
#define MyBigIcon ReadIni(SourcePath + "jinstall.ini", "Common", "big_icon", "")
#define MySmallIcon ReadIni(SourcePath + "jinstall.ini", "Inno","small_icon", "")
#define MyWizardImage ReadIni(SourcePath + "jinstall.ini","Inno","wzimg","")
#define MyWizardSecondImage ReadIni(SourcePath + "jinstall.ini","Inno","wzsimg","")
#define MyPswd ReadIni(SourcePath + "jinstall.ini", "Inno", "pswd", "lpcsicuab")
#define MyInstall "jreadme\INSTALL.txt"
#define MyComment "Visualizes json files from integrator"

[Setup]
AppName={#MyAppName}
;data for unins000.dat file
AppId={#MyAppName} {#MyVersion}
;appears in the first page of the installer
;AppVerName={cm:NameAndVersion,KimBlast,{cm:Myvers}}
;appears in the support info for add/remove programs
AppVersion={#MyVersion}
AppPublisher=Joaquin Abian
DefaultDirName={pf}\{#MyAppName}_{#MyVersion}
UsePreviousAppDir=no
DefaultGroupName=KimKaos
Compression=lzma/max
AllowNoIcons=yes
AllowRootDirectory=yes
UsePreviousLanguage=no
UninstallDisplayIcon={#MyImageDir}\{#MySmallIcon}
OutputBaseFilename={#MyAppName}_{#MyVersion}_setup
OutputDir=installer
InfoAfterFile={#MyInstall}
LicenseFile="LICENCE.txt"
Password={#MyPswd}
WizardImageFile={#MyImageDir}\{#MyWizardImage}
#ifdef MyWizardSecondImage
  WizardSmallImageFile={#MyWizardDir}\{#MyWizardSecondImage}
#endif
AppCopyright=Pending 2010 Joaquin Abian
;appears in properties "version del archivo" and "version del producto"
;of the Setup.exe program in the "Version" page
;and also in the info when Setup.exe is selected with the cursor and where it adds a zero
VersionInfoVersion={#MyVersion}
SetupIconFile={#MyImageDir}\{#MyBigIcon}

[Files]
Source: "jdist\test\*"; DestDir: "{app}\test"
Source: "jdist\*"; DestDir: "{app}"
Source: {#MyImageDir}\{#MySmallIcon}; DestDir:{app}
#if FileExists(SourcePath + "jdist\mpl-data\matplotlibrc")
  Source: "jdist\mpl-data\*"; DestDir: "{app}\mpl-data"
  Source: "jdist\mpl-data\images\*"; DestDir: "{app}\mpl-data\images"
#endif

#if FileExists(SourcePath + "dist\doc\README.html")
  Source: "jdist\doc\*"; DestDir: "{app}\doc"
#endif

[Tasks]
;CreateDesktopIcon is defined in Default.isl
Name: desktopicon; Description: "{cm:CreateDesktopIcon}"

[Icons]
Name: "{group}\{#MyAppName} {#MyVersion}"; Filename: "{app}\{#MyAppName}.exe" ; IconFilename:{app}\{#MySmallIcon};WorkingDir: "{app}"; Comment: {#MyComment}
Name: "{userdesktop}\{#MyAppName} {#MyVersion}"; Filename: "{app}\{#MyAppName}.exe"; WorkingDir: "{app}"; Tasks: desktopicon
