import wx
import numpy
import json
import os
from visor_main import Visor
from jvisor_fragmentor import get_ion_dict
from jvisor_data_panel import DataPanel
from visor_images import JICON
#
VISOR_TITLE = "  Visor JSON 3.1"
#
#
class JsonVisor(Visor):
    """"""
    def __init__(self, *args, **kwds):
        """"""
        kwds['data_panel'] = DataPanel
        kwds['title'] = VISOR_TITLE
        kwds['icon'] = JICON
        self.file = os.path.join(os.path.dirname(__file__), 'test/test.db')
        Visor.__init__(self, *args, **kwds)
        self.wildcard = "JSON (*.db)|*.db|" \
                        "Text (*.txt)|*.txt|"
        self.ext = '.db'
    #
    def get_data(self):
        """"""
        print(self.file)
        with open(self.file) as handle:
            self.data = json.load(handle)
    #
    def get_spectrum(self):
        """Build mass and intensity arrays"""
        data_spectrum = self.data[self.spectrum_file]['spectral_info']

        try:
            intensity_string = data_spectrum['intensity_array']
            mass_string = data_spectrum['mass_array']
        except KeyError:  #no spectrum defined
            intensity_string = '0'
            mass_string = '0'

        self.intensities = numpy.array(intensity_string.split('|'), dtype='float')
        self.masses = numpy.array(mass_string.split('|'), dtype='float')
    #
    def get_parameters(self):
        """Extract several attributes of the spectrum

        Extracts precursor mass, peptide_consensus, and parent charges

        """
        data_general = self.data[self.spectrum_file]
        self.precursor = data_general['spectral_info'].get('parent_mass', '0')
        self.peptide_consensus = data_general.get('consensus_mod', '')
        self.charges = int(data_general.get('consensus_z', '0'))
    #
    def identify_ions(self):
        """Set fragment type names for the spectrum ions.

        fragment_masses = expected fragment masses
        ion_mass = ion mass

        """
        precision = self.precision if self.precision else 0.3

        ion_dict = get_ion_dict(self.peptide_consensus, self.charges)
        fragment_masses = sorted(ion_dict.keys())
        ion_number = len(self.masses)
        color = 'black'
        self.colors = ['k'] * ion_number
        self.annotations = []
        #
        is_last = False
        fragment = ""
        last_annotated = 0
        fragment_mass = fragment_masses.pop(0)
        for ion_index, (ion_mass, ion_intensity) in \
                                 enumerate(zip(self.masses, self.intensities)):
            if (fragment_mass - ion_mass) > precision:
                continue
            elif abs(ion_mass-fragment_mass) < precision:
                if abs(ion_mass - last_annotated) > 1:
                    while fragment_masses:
                        fragment += ' %s' % ion_dict[fragment_mass]
                        fragment_mass = fragment_masses.pop(0)
                        if abs(ion_mass-fragment_mass) > precision: break
                    last_annotated = ion_mass
            else:
                try:
                    next_mass = fragment_masses.pop(0)
                except IndexError:
                    is_last = True
                else:
                    while ion_mass > next_mass and fragment_masses:
                        next_mass = fragment_masses.pop(0)
                    fragment_mass = next_mass

            if fragment:
                #
                if 'y' in fragment:
                    color = 'red'
                elif 'b' in fragment:
                    color = 'blue'
                elif 'M' in fragment:
                    color = 'green'
                    #
                self.annotations.append((fragment, color,
                                         ion_mass, ion_intensity))
                self.colors[ion_index] = color
                if is_last: return
                fragment = ""
    #
    def fill_info(self):
        """Write data on the visor text boxes"""
        data_general = self.data[self.spectrum_file]
        data_omssa = data_general.get('omssa', [])
        data_ascore = data_general.get('ascore_data', [])
        data_sequest = data_general.get('sequest', [])
        data_phenyx = data_general.get('phenyx', [])
        #
        try:
            self.peptide_from_ascore = data_ascore['peptide']
        except (KeyError, TypeError):
            self.peptide_from_ascore = ''
            #
        if 'itraq_info' in data_general:
            label_info = 'itraq_info'
        elif 'tmt_info' in data_general:
            label_info = 'tmt_info'
        else:
            label_info = ''
            #
        self.write_box(data_general, 'tc_gi', False,
            'consensus_deltamz', 'consensus_z',
            'ms', label_info, 'consensus','consensus_proteins')
        self.write_box(data_omssa, 'tc_oi', True,
            'modified_sequence', 'pvalue', 'expect')
        self.write_box(data_ascore, 'tc_ai', False,
            'peptide', 'peptide_score', 'ascore')
        self.write_box(data_phenyx, 'tc_pi', True,
            'modified_sequence', 'zscore', 'validity')
        self.write_box(data_sequest, 'tc_bi', True,
            'modified_sequence', 'xcorr', 'deltacn', 'd')
        #
        #fill additional data_general and ascore
        try:
            precursor = 'Precursor m/z : %.3f\n' % float(self.precursor)
        except AttributeError:
            precursor = 'Precursor m/z : n/a\n'
            #
        text = precursor + "final_consensus: %s\n"
        #
        if self.peptide_from_ascore:
            self.data_pan.tc_gi.WriteText(text % self.peptide_from_ascore)
            if self.peptide_from_ascore == self.peptide_consensus:
                self.data_pan.tc_ai.WriteText("Reassigned : No\n")
            else:
                self.data_pan.tc_ai.WriteText("Reassigned : Yes\n")
        else:
            self.data_pan.tc_gi.WriteText(text % self.peptide_consensus)
        #

#
#
#
if __name__ == '__main__':

    app = wx.App()
    jvisor = JsonVisor(None)
    jvisor.Show()
    #noinspection PyUnresolvedReferences
    app.MainLoop()
