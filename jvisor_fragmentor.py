#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#
"""
jvisor_fragmentor (visor_007)
1 de agosto de 2010
"""
#
#
from commons.mass import mass_aa, mass_group, mass_atom
#
mass_aa['737'] = mass_group['tmt']        #TMT
mass_aa['214'] = mass_group['itrq']       #itraq
mass_aa['23'] = (-18.0153, -18.010565)    #dehydratation
mass_aa['21'] = mass_group['Pho']         #phosphorylation
mass_aa['35'] = mass_group['Oxg']
mass_aa['C'] = mass_aa['c']             #reassign to cysteine the Cys-CAM value
#
#
def sequence_to_masses(peptide):
    """Transforms an amino acid sequence in a list of masses.

    - Peptides of type "K(737,737)RT(23)PPLLYR"
    - Admits PTMs as lower case codes

    """
    masses = []
    key = []

    for item in peptide:
        if item.isalpha():
            try:
                masses.append(mass_aa[item][1])
            except KeyError:
                print('rare AA ->%s<-' % item)
        else:
            if item == '(' :
                #noinspection PyUnusedLocal
                mass = 0
                continue
            if item == ')' :
                mass = get_ptm_mass_from_keys(key)
                key = []
                masses[-1] += mass
            else:
                key.append(item)

    return masses
#
#
def get_ptm_mass_from_keys(key):
    """Calculates ptm mass from a list of characters coding the ptm

    - key is like ['7','3','7',',','7','3','7']
    - becomes a list of ptm references ['737', '737']

    """
    mass = 0
    ptm_string = ''.join(key)
    ptms = ptm_string.split(',')

    for ptm in ptms:
        mass += mass_aa[ptm][1]

    return mass
#
#
def get_b_series(peptide):
    """b_series."""
    mass = mass_atom['H'][1]
    series = []
    for item in peptide:
        mass += item
        series.append(mass)
    return series
#
#
def get_y_series(peptide):
    """y_series."""
    mass = mass_atom['H'][1] + mass_group['H2O'][1]
    series = []
    peptide = reversed(peptide)
    for item in peptide:
        mass += item
        series.append(mass)
    return series
#
#
def make_names(series, name, charges):
    """Dictionary of theoretical fragments for a given series.

    dict_series: key   -> fragment mass
                 value -> fragment name.
    This should be the reverse because now the key is not necessarily unique

    """
    mass_H = mass_atom['H'][1]
    dict_series = {}
    for number, ion in enumerate(series):
        dict_series[ion] = '%s%i' % (name, number + 1)
        if charges > 2:
            dict_series[(ion + mass_H) / 2] = '%s%i+2' %(name, number + 1)

    return dict_series
#
#
def make_special_names(series, peptide, charges):
    """Dictionary of precursor ions.

     takes into account: phosphate and water loses and itraq/tmt breakdown.

     """
    dict_special = {}
    mass_H = mass_atom['H'][1]
    protons_mass = mass_H * charges
    parent = max(series) - mass_H
    precursor = (parent + protons_mass) / charges

    dict_special[precursor] = 'M+%i' % charges
    dict_special[(parent + protons_mass-mass_group['H2O'][1]) / charges
                ] = 'M-18+%i' % charges

    if '(21)' in peptide:           # phosphorylated
        dict_special[(parent + protons_mass - 97.976896) / charges
                    ] = 'M-P+%i' % charges


    if '(214)' in peptide:          #itraq
        dict_special[(parent + (mass_H * (charges - 1)) - 144.102063) / (charges - 1)
                    ] = 'M-itrq+%i' % (charges - 1)

    return dict_special
#
#
def get_ion_dict(peptide, charges=3, test=False):
    """Dictionary of theoretical fragments for a given sequence.

    - key is the mass of each fragment (float)
    - value is the type of fragment (p.e. 'y3+2')

    """
    peptide_masses = sequence_to_masses(peptide)
    y_series = get_y_series(peptide_masses)
    b_series = get_b_series(peptide_masses)
    #
    full = {}
    full.update(make_names(y_series, 'y', charges))
    full.update(make_names(b_series, 'b', charges))
    #
    special_names = make_special_names(y_series, peptide, charges)
    full.update(special_names)
    #
    if test:
        print(peptide)
        print("  y ions   b ions")
        for y, b in zip(y_series, reversed(b_series)):
            print("%8.2f %8.2f" % (y, b))

        masses = sorted(full.keys())
        print("")
        for mass in masses:
            print('%9s = %7.3f' %(full[mass], mass))

    return full


if __name__ == "__main__":

    peptide = "ASPVTXXTWIDsMHYCR"
    peptide = "K(737,737)RT(23)PPLLYR"
    peptide = "ASPVTS(23)XXTWIDS(21)MHYCR"
    #peptide = "ASPVTS(23)XXTWIDS(21)MHYcR"
    peptide = "ASDFT(21)R"

    get_ion_dict(peptide, 2, test=True)


##ASPVTS(23)XXTWIDS(21)MHYCR
##precursor = 733.658671
##
##  y ions   b ions
##  175.12  2180.97
##  335.15  2024.86
##  498.21  1864.83
##  635.27  1701.77
##  766.31  1564.71
##  933.31  1433.67
## 1048.34  1266.67
## 1161.42  1151.65
## 1347.50  1038.56
## 1448.55   852.48
## 1561.63   751.43
## 1674.72   638.35
## 1743.74   525.27
## 1844.79   456.25
## 1943.85   355.20
## 2040.91   256.13
## 2127.94   159.08
## 2198.98    72.04
##
##     b1+2 =   36.53
##       b1 =   72.04
##     b2+2 =   80.04
##     y1+2 =   88.06
##     b3+2 =  128.57
##       b2 =  159.08
##     y2+2 =  168.08
##       y1 =  175.12
##     b4+2 =  178.10
##     b5+2 =  228.63
##     y3+2 =  249.61
##       b3 =  256.13
##     b6+2 =  263.14
##     y4+2 =  318.14
##     b7+2 =  319.68
##       y2 =  335.15
##       b4 =  355.20
##     b8+2 =  376.22
##     y5+2 =  383.66
##     b9+2 =  426.74
##       b5 =  456.25
##     y6+2 =  467.16
##       y3 =  498.21
##    b10+2 =  519.78
##     y7+2 =  524.67
##       b6 =  525.27
##    b11+2 =  576.33
##     y8+2 =  581.21
##    b12+2 =  633.84
##       y4 =  635.27
##       b7 =  638.35
##     y9+2 =  674.25
##    M-P+3 =  701.00
##    b13+2 =  717.34
##    y10+2 =  724.78
##   M-18+3 =  727.66
##      M+3 =  733.66
##       b8 =  751.43
##       y5 =  766.31
##    y11+2 =  781.32
##    b14+2 =  782.86
##    y12+2 =  837.86
##    b15+2 =  851.39
##       b9 =  852.48
##    y13+2 =  872.37
##    y14+2 =  922.90
##    b16+2 =  932.92
##       y6 =  933.31
##    y15+2 =  972.43
##    b17+2 = 1012.94
##    y16+2 = 1020.96
##      b10 = 1038.56
##       y7 = 1048.34
##    y17+2 = 1064.47
##    b18+2 = 1090.99
##    y18+2 = 1099.99
##      b11 = 1151.65
##       y8 = 1161.42
##      b12 = 1266.67
##       y9 = 1347.50
##      b13 = 1433.67
##      y10 = 1448.55
##      y11 = 1561.63
##      b14 = 1564.71
##      y12 = 1674.72
##      b15 = 1701.77
##      y13 = 1743.74
##      y14 = 1844.79
##      b16 = 1864.83
##      y15 = 1943.85
##      b17 = 2024.86
##      y16 = 2040.91
##      y17 = 2127.94
##      b18 = 2180.97
##      y18 = 2198.98
##
##C:\Python26\programas\visor_006>
