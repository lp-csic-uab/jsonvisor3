#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
# generated by wxGlade HG on Sat Jul 24 21:56:52 2010

import wx

# begin wxGlade: extracode
# end wxGlade

import os
if os.name == 'posix':
    from wx.lib.stattext import GenStaticText as wxStaticText
elif os.name == 'nt':
    wxStaticText = wx.StaticText


class DataPanel(wx.Panel):
    def __init__(self, *args, **kwds):
        # begin wxGlade: DataPanel.__init__
        kwds["style"] = wx.TAB_TRAVERSAL
        wx.Panel.__init__(self, *args, **kwds)
        #self.lg_gi = wx.wxStaticText(self, -1, "General Info", style=wx.ALIGN_CENTRE)
        self.lg_gi = wxStaticText(self, -1, "General Info", style=wx.ALIGN_CENTRE)
        self.tc_gi = wx.TextCtrl(self, -1, "", style=wx.TE_MULTILINE|wx.HSCROLL)
        self.lg_ai = wxStaticText(self, -1, "Ascore", style=wx.ALIGN_CENTRE)
        self.tc_ai = wx.TextCtrl(self, -1, "", style=wx.TE_MULTILINE)
        self.lg_bi = wxStaticText(self, -1, "Sequest", style=wx.ALIGN_CENTRE)
        self.tc_bi = wx.TextCtrl(self, -1, "", style=wx.TE_MULTILINE)
        self.lg_oi = wxStaticText(self, -1, "Omssa", style=wx.ALIGN_CENTRE)
        self.tc_oi = wx.TextCtrl(self, -1, "", style=wx.TE_MULTILINE)
        self.lg_pi = wxStaticText(self, -1, "Phenyx", style=wx.ALIGN_CENTRE)
        self.tc_pi = wx.TextCtrl(self, -1, "", style=wx.TE_MULTILINE)
        self.lb_db = wxStaticText(self, -1, "Json File")
        self.tc_file = wx.TextCtrl(self, -1, "")
        self.bt_file = wx.Button(self, -1, "get")
        self.lbx_spectra = wx.ListBox(self, -1, choices=["NoFile"], style=wx.LB_HSCROLL)
        self.lb_search = wxStaticText(self, -1, "Search", style=wx.ALIGN_CENTRE)
        self.tc_search = wx.TextCtrl(self, -1, "")
        self.spb_search = wx.SpinButton(self, -1 , style=wx.SP_VERTICAL)

        self.__set_properties()
        self.__do_layout()
        # end wxGlade

    def __set_properties(self):
        # begin wxGlade: DataPanel.__set_properties
        self.SetBackgroundColour( wx.Colour(255, 127, 0) )
        violet = wx.Colour(187, 185, 255)
        self.lg_gi.SetBackgroundStyle(wx.BG_STYLE_COLOUR)
        self.lg_gi.SetBackgroundColour(violet)
        self.lg_ai.SetBackgroundColour(violet)
        self.lg_bi.SetBackgroundColour(violet)
        self.lg_oi.SetBackgroundColour(violet)
        self.lg_pi.SetBackgroundColour(violet)
        self.bt_file.SetMinSize( (35, 23) )
        self.lbx_spectra.SetSelection(0)
        self.lb_search.SetBackgroundColour(violet)
        self.spb_search.SetMinSize( (-1, 23) )
        # end wxGlade

    #noinspection PyArgumentList
    def __do_layout(self):
        # begin wxGlade: DataPanel.__do_layout
        sizer_1 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_4 = wx.BoxSizer(wx.VERTICAL)
        sizer_5 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_6 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_3 = wx.BoxSizer(wx.VERTICAL)
        sizer_2 = wx.BoxSizer(wx.VERTICAL)
        sizer_2.Add(self.lg_gi, 0, wx.EXPAND, 0)
        sizer_2.Add(self.tc_gi, 1, wx.EXPAND, 0)
        sizer_2.Add(self.lg_ai, 0, wx.EXPAND, 0)
        sizer_2.Add(self.tc_ai, 1, wx.EXPAND, 0)
        sizer_1.Add(sizer_2, 1, wx.LEFT|wx.TOP|wx.BOTTOM|wx.EXPAND, 6)
        sizer_3.Add(self.lg_bi, 0, wx.EXPAND, 0)
        sizer_3.Add(self.tc_bi, 1, wx.EXPAND, 0)
        sizer_3.Add(self.lg_oi, 0, wx.EXPAND, 0)
        sizer_3.Add(self.tc_oi, 1, wx.EXPAND, 0)
        sizer_3.Add(self.lg_pi, 0, wx.EXPAND, 0)
        sizer_3.Add(self.tc_pi, 1, wx.EXPAND, 0)
        sizer_1.Add(sizer_3, 1, wx.ALL|wx.EXPAND, 6)
        sizer_6.Add(self.lb_db, 0, wx.RIGHT|wx.ALIGN_CENTER_VERTICAL, 3)
        sizer_6.Add(self.tc_file, 1, wx.EXPAND|wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_6.Add(self.bt_file, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_4.Add(sizer_6, 0, wx.EXPAND, 0)
        sizer_4.Add(self.lbx_spectra, 1, wx.EXPAND, 0)
        sizer_4.Add(self.lb_search, 0, wx.EXPAND, 0)
        sizer_5.Add(self.tc_search, 1, wx.TOP, 1)
        sizer_5.Add(self.spb_search, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_4.Add(sizer_5, 0, wx.EXPAND, 0)
        sizer_1.Add(sizer_4, 1, wx.RIGHT|wx.TOP|wx.BOTTOM|wx.EXPAND, 6)
        self.SetSizer(sizer_1)
        sizer_1.Fit(self)
        # end wxGlade

# end of class DataPanel


