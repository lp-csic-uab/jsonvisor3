JsonVisor vs 0.7 se ha instalado con exito.

  JsonVisor ha sido programado en Python
      por Joaquin Abian

      Agosto 2010

*** binary dependencies ***
Your executable(s) also depend on these dlls which are not included,
you may or may not need to distribute them.

Make sure you have the license if you distribute any of them, and
make sure you don't distribute files belonging to the operating system.

   OLEAUT32.dll - C:\WINDOWS\system32\OLEAUT32.dll
   USER32.dll - C:\WINDOWS\system32\USER32.dll
   POWRPROF.dll - C:\WINDOWS\system32\POWRPROF.dll
   MPR.dll - C:\WINDOWS\system32\MPR.dll
   WINMM.dll - C:\WINDOWS\system32\WINMM.dll
   ADVAPI32.dll - C:\WINDOWS\system32\ADVAPI32.dll
   msvcrt.dll - C:\WINDOWS\system32\msvcrt.dll
   WS2_32.dll - C:\WINDOWS\system32\WS2_32.dll
   GDI32.dll - C:\WINDOWS\system32\GDI32.dll
   IMM32.dll - C:\WINDOWS\system32\IMM32.dll
   NETAPI32.dll - C:\WINDOWS\system32\NETAPI32.dll
   fftpack_lite.pyd - C:\Python27\lib\site-packages\numpy\fft\fftpack_lite.pyd   (*)
   mfc90.dll - C:\Python27\lib\site-packages\Pythonwin\mfc90.dll                 (*) 
   KERNEL32.dll - C:\WINDOWS\system32\KERNEL32.dll
   gdiplus.dll - C:\Python27\lib\site-packages\wx-2.8-msw-unicode\wx\gdiplus.dll (*)
   WSOCK32.dll - C:\WINDOWS\system32\WSOCK32.dll
   VERSION.dll - C:\WINDOWS\system32\VERSION.dll
   ole32.dll - C:\WINDOWS\system32\ole32.dll
   SHELL32.dll - C:\WINDOWS\system32\SHELL32.dll
   RPCRT4.dll - C:\WINDOWS\system32\RPCRT4.dll
   COMDLG32.dll - C:\WINDOWS\system32\COMDLG32.dll
   COMCTL32.dll - C:\WINDOWS\system32\COMCTL32.dll
   WINSPOOL.DRV - C:\WINDOWS\system32\WINSPOOL.DRV
   MSVCP90.dll - C:\Python27\programas\_visor\MSVCP90.dll    (*)
   
   
 (*) provided


*** JsonVisor Licence ***

      This program is free software; you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation; either version 3 of the License, or
      (at your option) any later version.
      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program; if not, write to the Free Software

      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
      MA 02110-1301, USA.

See LICENCE.txt for more details about licensing of the software (GPL v3).


