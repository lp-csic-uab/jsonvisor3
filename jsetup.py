# exWx/setup.py
from distutils.core import setup
import os, sys
import matplotlib
import ConfigParser
#noinspection PyUnresolvedReferences
import py2exe
#
sys.argv.append('py2exe')
config = ConfigParser.ConfigParser()
config.readfp(open('jinstall.ini'))
#
PyName = config.get('Setup', 'pyname')
AppVersion = config.get('Common', 'version')
AppName = config.get('Common', 'name')
Icondir = config.get('Common', 'imgdir')
Icon = config.get('Common', 'big_icon')
#
Icon = os.path.join(Icondir, Icon)
#
def get_docs():
    folder = 'doc_jvisor'
    docs = []
    for arch in os.listdir(folder):
        docs.append(os.path.join(folder, arch))
    return [("doc", docs)]
#
#
####################################
#
#      jvisor.pyw
#
####################################
#
#
setup(
    windows = [
              {'script': PyName,
               'icon_resources':[(0, Icon)],
               'dest_base' : AppName,
               'version' : AppVersion,
               'company_name' : "JoaquinAbian",
               'copyright' : "No Copyrights",
               'name' : AppName 
              }
              ],
            
    options = {
              'py2exe': {
                        'packages' :    ['matplotlib', 'pytz'],
##                        'includes':     [],
                        'excludes':     ['EasyDialogs', 'PyQt4',
                                         'bsddb', 'curses', 'Pyrex',
                                         'email', 'testing', 'fft',
                                         '_gtkagg', '_tkagg', 'pywin.debugger',
                                         'pywin.debugger.dbgcon', 'pywin.dialogs',
                                         'Tkconstants','Tkinter', 'tcl'
                                        ],
                        'ignores':       ['wxmsw26uh_vc.dll'],
                        'dll_excludes': ['libgdk_pixbuf-2.0-0.dll',
                                         'libgdk-win32-2.0-0.dll',
                                         'libgobject-2.0-0.dll',
                                         'tk84.dll','tcl84.dll'
                                        ],
                        'compressed': 1,
                        'optimize':2,
                        'bundle_files': 1,
                        'dist_dir': "jdist"
                        }
              },
    zipfile = None,
    #Additional files I want in folder  'dist'.
    #The packer (inno) can go to the main dir to get them but then
    #they would not be in 'dist' and the program.exe there would not work.
    data_files = get_docs() + \
                [("", ["jreadme\\INSTALL.txt",    #Installation final page
                        "jreadme\\README.txt",    #README and help (English)
                        #"README.ESP.txt",   #README and help (Spanish)
                        #"README.CAT.txt",   #README and help (Catalan)
                        "LICENCE.TXT",  #Licence File (GPL v3)
                        "msvcp90.dll",
                        "C:\\Python27/Lib/site-packages/wx-2.8-msw-unicode/wx/gdiplus.dll",
                        "C:\\Python27/lib/site-packages/numpy/fft/fftpack_lite.pyd",
                        "C:\\Python27/lib/site-packages/Pythonwin/mfc90.dll"
                        ]
                  ),
                  ("test", ["test/test.db"]),
                 ] + matplotlib.get_py2exe_datafiles(), requires=['wx']
    )
